import { shallowMount } from '@vue/test-utils';
import PostMaker from '../../src/components/PostMaker/PostMaker.vue';

describe('test PostMaker', () => {
  test('everything is in position', () => {
    const wrapper = shallowMount(PostMaker);
    expect(wrapper.find('#postInput').exists()).toBeTruthy();
    expect(wrapper.find('.post-maker__input-show').exists()).toBeTruthy();
    expect(wrapper.find('.post-maker__submit').exists()).toBeTruthy();
  });
  test('text area text', async (done) => {
    const wrapper = shallowMount(PostMaker);
    const postTextValue = 'Lorem ipsum dolor';
    await wrapper.setData({ postText: postTextValue });
    expect(wrapper.find('#postInput').element.value).toBe(postTextValue);
    expect(wrapper.find('.post-maker__input-show p').text()).toBe(postTextValue);
    done();
  });
  test('Show hashtag', async (done) => {
    const wrapper = shallowMount(PostMaker);
    const postTextValue = 'Lorem ipsum #dolor';
    await wrapper.setData({ postText: postTextValue });
    expect(new Set(wrapper.find('.post-maker__input-show p .post-maker__input-show__word--hashtag').classes())).toEqual(new Set(['post-maker__input-show__word', 'post-maker__input-show__word--hashtag']));
    done();
  });
});
