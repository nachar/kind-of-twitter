import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home/Home.vue';
import Hashtags from '../views/Hashtags/Hashtags.vue';
import Posts from '../views/Post/Posts.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/hashtags',
    name: 'Hashtags',
    component: Hashtags,
  },
  {
    path: '/posts/:hashtag',
    name: 'Post',
    component: Posts,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
