export default {
  name: 'MixinGlobal',
  methods: {
    splitWords(text) {
      if (text === '') return [];
      return text.split(' ');
    },
    getLastWord(text) {
      const words = this.splitWords(text);
      if (words.length === 0) return '';
      return words[words.length - 1];
    },
    checkHashtag(word) {
      return /^#[^ !@#$%^&*(),.?":{}|<>]*$/gi.test(word);
    },
    cleanHashtag(word) {
      return word.substring(1);
    },
    deepClone(element) {
      return JSON.parse(JSON.stringify(element));
    },
  },
};
