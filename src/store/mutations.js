export default {
  ADD_POST(state, post) {
    state.posts.push(post);
  },
  ADD_HASHTAGS(state, hashtags) {
    state.tags = [...state.tags, ...hashtags];
  },
};
